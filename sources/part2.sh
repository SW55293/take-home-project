#!/bin/bash

#2- 
#Pick a LAN device (e.g. ethernet) from the list. Do not pick a pseudo device, but pick a real device which is
#attached to the real live network. This may be a real physical ethernet device or wifi device.

while IFS= read -r line; 
	do
    interface_line=$(echo "$line" | awk '{print $1}')
    if [[ "$interface_line" != "l*" ]]; then
     
      network_interface=$interface_line

    fi
done < <(ifconfig | grep "UP\|RUNNING\|BROADCAST")


network_interface=$(echo $network_interface | awk -F ":" '{print $1}')
#echo $network_interface


