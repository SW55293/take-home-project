## project description

This project has one main script called run.sh. This script outputs the pinged ip addresses and checks who responded, the responces are then transferred to a text file.

# what it does
It loops through a starting ip address and receives a responce. 

# how to build and run the project

    1. open up the terminal and cd into where you want the script to be
    2. run a nano command with the name run.sh 
    3. in the script type in your shebang, for loop, the range of ip addresses you're going to iterate through, and finally the file name you want the output in
    4.  once script is saved you run this command to make file executable chmod +x run.sh
    5. then you execute the script with ./run.sh and wait for to iterate through all the ip addresses.