#! /bin/bash

network_id=$(echo $ip | awk -F '.' '{print $1 "."$2 "."$3}')
echo "My Network ID: $network_id.1"
echo "#####################################"
echo
echo "Scanning all ip's in the network"

for ip in {1..255};
do
  current_ip=$network_id.$ip
  if ping -c 1 $current_ip > /dev/null
      then
        echo "Ping to $current_ip: OK"
  else
        echo "Ping to $current_ip: FAIL"
  fi
done >> pinged.txt