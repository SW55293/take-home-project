# Take Home Project

## Challenge 1 : check IP of devices connected in a network

1. List information on all network devices on your machine. IP addresses, link-level addresses, network masks, etc. per network device on your machine.
2. Pick a LAN device (e.g. ethernet) from the list. Do not pick a pseudo device, but pick a real device which is
attached to the real live network. This may be a real physical ethernet device or wifi device.
3. Find the IP range that are possible on that LAN.  For example, if your device chosen in step #2 was called "eth1" and it had IP address 192.168.1.100 and network mask of 255.255.255.0, then you will probably
use a range of 192.168.1.1 through 192.168.1.254
4. write a simple script that will iterate through IP address range (for example, 192.168.1.1 through 192.168.1.254)
and ping each once and see if response was received successfully and finally print out a list of IP addresses of the machines that responded on your local network.

### Rundown:
    1. Run ipconfig or ifconfig command to show you all network information
    2. Pick a device that is live. To make sure it's live run ping <your ip live address > if you receive a host unreachable or a packet loss you should choose a different address.
    3. Once you have the ip address we then find the range 
    4. Example range could be from 192.168.86.34 to 192.168.86.254
    5. We can now loop through the starting ip to the ending ip range and output the failed or successfull pings in a txt file

## Example Output:
-Pinging 192.168.86.54 with 32 bytes of data:

    Reply from 192.168.86.34: Destination host unreachable.

    Reply from 192.168.86.34: Destination host unreachable.

    Reply from 192.168.86.34: Destination host unreachable.

    Reply from 192.168.86.34: Destination host unreachable.







